package com.example.com.msit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Администратор on 17.08.2016.
 */
public class SessionToken {
    @SerializedName("session_token")
    @Expose
    private String session_token;

    public String getSession_token() {
        return session_token;
    }

    public void setSession_token(String session_token) {
        this.session_token = session_token;
    }
}
