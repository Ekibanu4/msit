package com.example.com.msit;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Администратор on 17.08.2016.
 */
public class UserData {
    @SerializedName("cod")
    @Expose
    @Nullable
    private int cod;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    @SerializedName("created_at")
    @Expose
    @Nullable
    private String created_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    @SerializedName("phone_number")
    @Expose
    private String phone_number;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("date_of_birth")
    @Expose
    private String date_of_birth;

    @SerializedName("address_of_birth")
    @Expose
    private String address_of_birth;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("status")
    @Expose
    private String status;


    @SerializedName("reject_reason")
    @Expose
    private String reject_reason;

    @SerializedName("document_photos")
    @Expose
    @Nullable
    private List<Bitmap> document_photos;


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getAddress_of_birth() {
        return address_of_birth;
    }

    public void setAddress_of_birth(String address_of_birth) {
        this.address_of_birth = address_of_birth;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReject_reason() {
        return reject_reason;
    }

    public void setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
    }

    public List<Bitmap> getDocument_photos() {
        return document_photos;
    }

    public void setDocument_photos(List<Bitmap> document_photos) {
        this.document_photos = document_photos;
    }
}
