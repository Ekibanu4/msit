package com.example.com.msit.api;

import com.example.com.msit.UserData;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Администратор on 17.08.2016.
 */
public class LoginApi {

    public interface GetUserData {
        @POST("api/v1/login")
        Call<UserData> getUserData(@Query("phone_number") String phone
//                @QueryMap Map<String, String> options
        );

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://31.131.16.243:83/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
