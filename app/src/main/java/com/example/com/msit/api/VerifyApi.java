package com.example.com.msit.api;

import com.example.com.msit.SessionToken;
import com.example.com.msit.UserData;
import com.example.com.msit.secret.SecretContract;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Администратор on 17.08.2016.
 */
public class VerifyApi {

    public interface VerifyCode {
        @POST("api/v1/verify")
        Call<SessionToken> VerifyCode(@Query(value = "phone_number") String phone, @Query("verification_code") String verification_code

        );

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://31.131.16.243:83/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
