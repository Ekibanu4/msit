package com.example.com.msit.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.com.msit.R;
import com.example.com.msit.secret.SecretActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Администратор on 16.08.2016.
 */
public class LoginActivity extends AppCompatActivity implements LoginContract.View {
    @BindView(R.id.login_button)
    Button button_Login;
    @BindView(R.id.edit_phone)
    EditText edit_Phone;
    @BindView(R.id.text_info)
    TextView text_info;
    @BindView(R.id.login_image_logo)
    ImageView image_logo;

    LoginPresenter loginPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this);

    }


    @OnClick(R.id.login_button)
    public void loginButtonClick() {
            String phone = edit_Phone.getText().toString();
            progressDialog = ProgressDialog.show(this, getString(R.string.login_authenticate), null);
            loginPresenter.tryLogin(phone);
    }


    @Override
    public void phoneCheckFailed() {
        progressDialog.dismiss();
        Toast.makeText(LoginActivity.this, R.string.login_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void redirectToSecret() {
        progressDialog.dismiss();
        Intent intent = new Intent(this, SecretActivity.class);
        intent.putExtra("phone",edit_Phone.getText().toString());
        startActivity(intent);
    }

}