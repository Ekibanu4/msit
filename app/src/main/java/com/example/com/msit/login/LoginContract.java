package com.example.com.msit.login;

/**
 * Created by Администратор on 16.08.2016.
 */
public interface LoginContract {
    interface View {
        void phoneCheckFailed();
        void redirectToSecret();
    }

    interface UserAction {
        void tryLogin(String phone);
    }
}
