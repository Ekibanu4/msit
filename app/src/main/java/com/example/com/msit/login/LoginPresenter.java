package com.example.com.msit.login;

import com.example.com.msit.UserData;
import com.example.com.msit.api.LoginApi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Администратор on 16.08.2016.
 */
public class LoginPresenter implements LoginContract.UserAction {
    private final LoginContract.View loginContract;


    public LoginPresenter(LoginContract.View loginContract) {
        this.loginContract = loginContract;

    }

    @Override
    public void tryLogin(final String phone) {
        final boolean[] fail = {false};
        if (!(phone.length() == 0 || phone.length() > 13 || phone.length() < 13)) {

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, String> data = new HashMap<>();
                    data.put("phone_number", phone);
                    try {
                        LoginApi.GetUserData getUserData = LoginApi.GetUserData.retrofit.create(LoginApi.GetUserData.class);
                        Call<UserData> call = getUserData.getUserData(phone);

                        Response<UserData> userDataO = call.execute();
                        if (userDataO.raw().code() == 200) {
                            final UserData userData = userDataO.body();
                            LoginPresenter.this.loginContract.redirectToSecret();

                        } else {
                            fail[0] = true;
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                        fail[0] = true;
                    }
                }
            });

            t.start(); // spawn thread

            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
if (fail[0]){loginContract.phoneCheckFailed();}
        }
            else {
            loginContract.phoneCheckFailed();



    }
}}

