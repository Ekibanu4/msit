package com.example.com.msit.register;

import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.com.msit.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Администратор on 16.08.2016.
 */
public class RegisterActivity extends AppCompatActivity implements RegisterContract.View {
    @BindView(R.id.register_button_proceed)
    Button btn_send;
    @BindView(R.id.register_layour_take_picture)
    RelativeLayout layoutTakePicrute;
    @BindView(R.id.register_text_firstname)
    EditText text_firstName;
    @BindView(R.id.register_text_lastname)
    EditText text_lastName;
    @BindView(R.id.register_text_dateofbirth)
    EditText text_dateOfBirth;
    @BindView(R.id.register_text_placeofbirth)
    EditText text_placeOfBirth;
    @BindView(R.id.register_logo_image)
    ImageView image_logo;
    RegisterPresenter presenter;
    static final int REQUEST_IMAGE_CAPTURE = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        presenter=new RegisterPresenter(this);
        text_firstName.setText(getIntent().getStringExtra("session_token"));
    }



    @Override
    public void showErrorToast() {
        Toast.makeText(RegisterActivity.this, "Please fill all fields", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDateDialog() {


    }

    @Override
    public void takePictureClick() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @OnClick(R.id.register_button_proceed)
    public void sendButtonClick(){
        String firstName,lastname,birthDate,birthPlace;
        firstName = text_firstName.getText().toString();
        lastname = text_lastName.getText().toString();
        birthDate = text_dateOfBirth.getText().toString();
        birthPlace = text_placeOfBirth.getText().toString();
        presenter.checkStringInputs(firstName,lastname,birthDate,birthPlace);
    }

    @OnClick(R.id.register_layour_take_picture)
    public void take(){takePictureClick();}


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            image_logo.setImageBitmap(imageBitmap);
        }
    }
}
