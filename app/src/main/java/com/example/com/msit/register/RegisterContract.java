package com.example.com.msit.register;

import java.util.Date;

/**
 * Created by Администратор on 16.08.2016.
 */
public interface RegisterContract {
interface View{
//    void sendButtonClick();
    void showErrorToast();
    void showDateDialog();
    void takePictureClick();

}
    interface UserAction{
void checkStringInputs(String firstName, String lastName, String birthDate,String birthPlace);
    }
}
