package com.example.com.msit.register;

/**
 * Created by Администратор on 16.08.2016.
 */
public class RegisterPresenter implements RegisterContract.UserAction {
   RegisterContract.View registerContract;

    public RegisterPresenter(RegisterContract.View registerContract){
        this.registerContract = registerContract;
    }


    @Override
    public void checkStringInputs(String firstName, String lastName, String birthDate, String birthPlace) {
        boolean isOk = false;
        if(!(firstName.length()==0||lastName.length()==0||birthDate.length()==0||birthPlace.length()==0)){isOk=true;}
        if(isOk){}else{registerContract.showErrorToast();}
    }
}
