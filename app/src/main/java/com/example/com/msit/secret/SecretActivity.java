package com.example.com.msit.secret;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.com.msit.R;
import com.example.com.msit.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecretActivity extends AppCompatActivity implements SecretContract.View {
    @BindView(R.id.secretButtonNext)
    Button secretButtonNext;
    @BindView(R.id.secret_image_logo)
    ImageView secret_image_logo;
    @BindView(R.id.secret_text_info)
    TextView secret_text_info;
    @BindView(R.id.secret_text_SmsSendInfo)
    TextView secret_text_SmsSendInfo;
    @BindView(R.id.secret_edit_secret)
    EditText secret_edit_secret;

    ProgressDialog progressDialog;
    SecretPresenter secretPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secret);
        ButterKnife.bind(this);
        secretPresenter = new SecretPresenter(this);
    }

    @Override
    public void navigateToRegistration(String session_token) {
        progressDialog.dismiss();
        Intent intent=new Intent(this,RegisterActivity.class);
        intent.putExtra("session_token",session_token);
        startActivity(intent);
    }

    @Override
    public void secretError() {
        progressDialog.dismiss();
        Toast.makeText(SecretActivity.this, R.string.secretError, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.secretButtonNext)
    public void secretButtonNextClick() {
        String secret = secret_edit_secret.getText().toString();
        String phone = getIntent().getStringExtra("phone");
        progressDialog = ProgressDialog.show(this,getString(R.string.secret_checking_confirmation_code),null);
        secretPresenter.trySecret(secret,phone);

    }
}
