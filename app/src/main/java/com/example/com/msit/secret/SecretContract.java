package com.example.com.msit.secret;

/**
 * Created by Администратор on 16.08.2016.
 */
public interface SecretContract {
    interface View {
        void navigateToRegistration(String session_token);
        void secretError();
    }
    interface UserAction{
        void trySecret(String secret,String phone);
    }
}
