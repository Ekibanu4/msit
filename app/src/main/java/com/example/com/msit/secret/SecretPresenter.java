package com.example.com.msit.secret;

import com.example.com.msit.SessionToken;
import com.example.com.msit.UserData;
import com.example.com.msit.api.LoginApi;
import com.example.com.msit.api.VerifyApi;
import com.example.com.msit.login.LoginPresenter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Администратор on 16.08.2016.
 */
public class SecretPresenter implements SecretContract.UserAction {

    private SecretContract.View secretContract;


    public SecretPresenter(SecretContract.View secretContract){
        this.secretContract = secretContract;
    }


    @Override
    public void trySecret(final String secret, final String phone) {
        final boolean[] fail = {false};
        if (!(secret.length() == 0||secret.length()<8||secret.length()<8)) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, String> data = new HashMap<>();
                    data.put("phone_number", phone);
                    data.put("verification_code", secret);
                    try {
                        VerifyApi.VerifyCode getVerifyCode =  VerifyApi.VerifyCode.retrofit.create( VerifyApi.VerifyCode.class);
                        Call<SessionToken> call = getVerifyCode.VerifyCode(phone,secret);

                        Response<SessionToken> sessionTokenResponse = call.execute();
                        if (sessionTokenResponse.raw().code() == 200) {
                            final SessionToken sessionToken = sessionTokenResponse.body();
                            secretContract.navigateToRegistration(sessionToken.getSession_token());

                        } else {
                            fail[0] = true;
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                        fail[0] = true;
                    }
                }
            });

            t.start(); // spawn thread

            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (fail[0]){secretContract.secretError();}
        }
        else {
            secretContract.secretError();}



        if (fail[0]){secretContract.secretError();}


//            secretContract.navigateToRegistration();

    }


}
